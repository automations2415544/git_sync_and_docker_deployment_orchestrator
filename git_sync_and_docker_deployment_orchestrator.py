import asyncio
import git
import os
from dotenv import load_dotenv
import subprocess

def pull_request(local_directory):
    repo = git.Repo(local_directory)
    before_pull_commit = repo.head.commit
    try:
        origin = repo.remote()
        origin.pull()
        print(f"Pulling the latest changes in {local_directory}.")

        if before_pull_commit!=repo.head.commit:
            return True
        
    except git.exc.GitCommandError as e:
        print(f"Failed to pull changes in {local_directory}: {str(e)}")
    except git.exc.NoSuchPathError as e:
        print(f"Invalid path for repository {local_directory}: {str(e)}")
    return False

load_dotenv()

local_directories = os.getenv("LOCAL_DIRECTORY").split(',')

directories_with_changes = []

for local_directory in local_directories:
    directories_with_changes.append(pull_request(local_directory))

print(directories_with_changes)

if any(directories_with_changes[:3]):
    print("Changes detected in the following directories. Performing additional actions.")
    subprocess.run(["docker-compose", "down"], check=True, cwd=local_directories[0])
    subprocess.run(["docker-compose", "build"], check=True, cwd=local_directories[0])
    subprocess.run(["docker-compose", "up", "-d"], check=True, cwd=local_directories[0])
    subprocess.run(["docker","image","prune","-f"], check=True, cwd=local_directories[0])

    print("Additional actions completed.")
else:
    print("No additional actions required.")
